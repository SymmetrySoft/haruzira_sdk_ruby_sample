#!/usr/bin/ruby
# -*- coding: utf-8 -*-
#require "./HaruziraRequire"
require "haruzira_sdk"

class HzSampleDemo

    # <summary>
    # Construct
    # </summary>
    # <param name="callback1">call back function when received a 'Notify Complete Speech' message</param>
    # <param name="callback2">call back function when occurred a exception(except disconnect)</param>
    # <param name="callback3">call back function when occurred a disconnecting issue</param>
    # <param name="callback4">call back function when received a 'Send Speech Recognition Command' message</param>
    def initialize(callback1 = nil, callback2 = nil, callback3 = nil, callback4 = nil)
        #en:create instance of ClientTcpCommunication class（regist call back functions and others）
        #ja:クライアント通信処理インスタンスの生成（コールバック関数の登録）
        @cltTcpComm = ClientTcpCommunication.new
        @cltTcpComm.EvNotifyCompeteSpeech = callback1
        @cltTcpComm.EvNotifyMessageEvent = callback2
        @cltTcpComm.EvNotifyReceivedDisConnectEvent = callback3
        @cltTcpComm.EvNotifySendSpeechRecognitionCommand = callback4
        @serverPort = 46000         #port number for access point
        @serverIp = "192.168.1.2"     #ip address for access point
        @receivePort = 46100        #port number for recieving asynchronous messages
        @cltTcpComm.ReceivedDataDecryptionKey = ""        #decryption key for that be encrypted voice command
    end

    attr_accessor   :serverPort, :serverIp, :receivePort

    # <summary>
    # en:set a decryption key for received data
    # ja:受信データ複合キー設定
    # </summary>
    # <param name="key">decryption key</param>
    def rcvDataDecryptionKey=(key)
        #Set
        @cltTcpComm.ReceivedDataDecryptionKey = key
    end


#region sending messsage
    # <summary>
    # en:send a speech data
    # ja:読み上げデータ送信
    # </summary>
    # <param name="sendText">text data to speech</param>
    # <param name="account">account name</param>
    # <param name="passwd">password</param>
    # <param name="encKey">encryption key</param>
    def sendSpeechData(sendText, account = nil, passwd = nil, encKey = nil)

        begin
            #en:make a set if you need that output of trace messages.(true:output, false:don't output)
            #ja:トレース出力設定(true:出力, false:出力しない)
            @cltTcpComm.setTraceOutPut(false)
            #en:you must make a set some values for access point, and do it if you need receiving asynchronous messages.
            #ja:接続先及び非同期メッセージ受信情報設定
            @cltTcpComm.ServerPortNo = @serverPort      #port number for access point
            @cltTcpComm.ServerIP = @serverIp            #ip address for access point
            @cltTcpComm.ReceivePort = @receivePort      #port number for recieving asynchronous messages

            puts "Connecting"

            #en:make a set of speech options.
            #ja:読み上げオプションの設定
            @cltTcpComm.ReqSendDataAccountName = account
            @cltTcpComm.ReqSendDataPasswd = passwd  
            @cltTcpComm.ReqSendDataEncryptKey = encKey
            @cltTcpComm.ReqSendDataSpeechMode = HzSpeechTextMode::Text
            @cltTcpComm.ReqSendDataSpeechLevel = HzSpeechLevel::Normal
            #@cltTcpComm.ReqSendDataSpeechLocaleId = 0x0411
            @cltTcpComm.ReqSendDataSpeechLocaleId = 0x00
            @cltTcpComm.ReqSendDataSpeechGender = HzSpeechGender::Neutral
            @cltTcpComm.ReqSendDataSpeechAge = 0
            @cltTcpComm.ReqSendDataSpeechRepeat = 0
            @cltTcpComm.ReqSendDataText = sendText

            #en:send a speech data.
            #ja:読み上げデータ送信
            timeStamp = @cltTcpComm.sendSpeechDataEx()

            if (timeStamp == nil)
                puts("error code：[0x%02x]" % [@cltTcpComm.ReceiveStatus])
            end

            #en:the send result informations output
            #ja:送信時情報表示
            puts("time stamp[%s]" % [timeStamp])
            puts("sent data size：[%d]" % [@cltTcpComm.SendDataLength])
            puts("SDK Version:[%s]" % [@cltTcpComm.Version])


        rescue Exception => ex
            puts(ex)
        ensure
            puts "Disconnected"
        end
    end

    # <summary>
    # en:start a receiving thread for messages of asynchronous.
    # ja:非同期メッセージ受信スレッド(Listener)の起動
    # </summary>
    def startAsynchronousListener()
        @cltTcpComm.setTraceOutPut(false)
        @cltTcpComm.ReceivePort = @receivePort      #port number for recieving asynchronous messages
        @cltTcpComm.startAsynchronousListener()
    end

    # <summary>
    # en:cancel a receiving thread for messages of asynchronous.
    # ja:非同期メッセージ受信スレッド(Listener)の終了
    # </summary>
    def cancelAsynchronousListener()
        @cltTcpComm.setTraceOutPut(true)
        @cltTcpComm.cancelAsynchronousListener()
    end

    # <summary>
    # en:cancel sending a speech data.
    # ja:読み上げデータ送信の停止
    # </summary>
    def stopSendSpeechData()
        @cltTcpComm.setTraceOutPut(true)
        @cltTcpComm.stopSendSpeechData()
    end

    # <summary>
    # en:set and get, time out value to receive messasges of responce.
    # ja:応答受信時タイムアウト取得・設定
    # </summary>
    def receiveAckTimeOut()
        #get value.
        puts @cltTcpComm.ReceiveAckTimeOut.to_s
        #set value of 55 seconds. 
        @cltTcpComm.ReceiveAckTimeOut = 55
        #get value again.
        puts @cltTcpComm.ReceiveAckTimeOut.to_s 
    end

    # <summary>
    # en:set SpeechCompletionNotice Necessity.
    # ja:読み上げ完了通知の要否設定
    # </summary>
    def setCompletionNoticeNecessity(val)
        if(val == "y")
            @cltTcpComm.ReqSendDataCompletionNoticeNecessity = HzSpCompletionNoticeNecessity::Need
        else
            @cltTcpComm.ReqSendDataCompletionNoticeNecessity = HzSpCompletionNoticeNecessity::NoNeed
        end
    end

#endregion
end