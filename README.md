# Haruzira SDK sample for Ruby #

Haruzira is a UWP APP.

read readme_xx.txt

Haruzira has published in the Windows Store. https://www.microsoft.com/store/apps/9nblggh516j3

It is localized Japanese and English.

And the SDK User's Manual has published on the Web site. https://haruzirasdke.wpblog.jp/


---------------------------------
## Example programs ##
* 1) SendTextDemo.rb, HzSampleDemo;  
    this program has following features.  
    - sends text to haruzira.  
    - receives command from haruzira.  
    - sends rss news contents.  
    - try the sdk options.

### ###
* 2) SimpleDemo.rb;  
    this program has simple feature that it sends text message to haruzira.  
    you will be able to learn the simple message sending.

### ###
* 3) simple_command_receiver.rb;  
    this program has simple feature that it is received  sending command from haruzira.  
    you will be able to learn  the simple command receiving.  

### ###
* 4) voice_clock.rb:  
    this program sends current time to haruzira; and haruzira will playback it by voice synthesize.  
    and haruzira can control it by using voice recognition from remote.  
    * demo movie  
    japanese site: https://symmetry-soft.com/html/tips/tts/others/tips_tts_voiceclock.htm#dotnet_exec  
