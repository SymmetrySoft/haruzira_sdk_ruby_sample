#!/usr/bin/ruby
# -*- coding: utf-8 -*-
require "rss"
require "./HzSampleDemo"

###############################################################
# en:global variable.
# ja:グローバル変数
###############################################################
sampleDemo = nil    #for HzSampleDemo class instance


###############################################################
# en:Language message tables for stdout.
# ja:標準出力用言語テーブル
###############################################################
strings_en = {:input_data => "Input  speech text：", :irregular_num => "Inpted a irregular number\nInput a number again.\n", \
              :end_rss => "The thread of sending a rss data was aborted.", :stop_listener => "The listener was stopped.\n", \
              :cancel_send => "Sending a speech data was canceld.\n", :rec_comp => "Received, Speech Completion Notice.", \
              :rcv_command => "Received, Send Speech Recognition Command.\n", :rss_url => "https://www.voanews.com/api/zq$omekvi_", \
              :invalid_command => "Invalid command data received. The command '%s' can not any work.", :weather_msg => "The weather status at current time.\nTemperature:[2.0 degrees]\nHumidity:[45 percent]\nAir pressure:[598 hectopascal]", \
              :weather_command => "weather information", :my_locate => "I am on Mount Shasta.\n", \
              :completion_notice => "If you want to need the completion notice, enter 'y'. or else 'n'.\n" \
             }
strings_ja = {:input_data => "読み上げ文字列を入力して下さい。：", :irregular_num => "不正な番号が入力されました。\n再入力して下さい。\n", \
              :end_rss => "RSSニュースデータ送信スレッド終了", :stop_listener => "Listenerを終了しました。\n", \
              :cancel_send => "読み上げデータ送信を中断しました。\n", :rec_comp => "読み上げ完了通知を受信しました。", \
              :rcv_command => "音声認識コマンド送信メッセージを受信しました。", :rss_url => "http://www3.nhk.or.jp/rss/news/cat0.xml", \
              :invalid_command => "不明なコマンドを受信しました。コマンド「%s」に対する処理は実行できません。", :weather_msg => "現在の気象情報をお知らせします。\n気温「１０℃」\n湿度「５０％」\n気圧「980ヘクトパスカル」です。", \
              :weather_command => "気象状況", :my_locate => "こちらは、高尾山です。\n", \
              :completion_notice => "読み上げ完了通知の送信を期待する場合は'y', そうでなければ'n'を入力して下さい。\n" \
             }
#menu of english
menu_en = <<EOF
=============================================================================
    Menu for demo
=============================================================================
[0]:send a speech data          [1]:send a rss news data(5 minutes interval)
[2]:stop a listener             [3]:cancel sending a speech data
[4]:set and get, time out value [5]:SpeechCompletionNotice Necessity
[99]:quit(exit)
=============================================================================

Input a number：
EOF

#menu of japanese
menu_ja = <<EOF
=============================================================================
    デモ用メニュー
=============================================================================
[0]:読み上げデータ送信          [1]:RSSニュースデータ送信
[2]:Listener停止                [3]:読み上げデータ送信の停止（中断）
[4]:応答受信時タイムアウト値の取得・設定
[5]:読み上げ完了通知の要否
[99]:終了(QUIT)
=============================================================================

[番号]を入力して下さい：
EOF

# en:get a parameter to set language code
# ja:言語設定を行うためのパラメータ取得
strings = strings_en
lang = ARGV[0].to_s.downcase 
if lang == "ja"
    menu = menu_ja
    strings = strings_ja
else
    menu = menu_en
end



###############################################################
# en:call back functions when occurred issues on connecting
# ja:通信イベントコールバック関数
###############################################################
# <summary>
# en:notify when occurred issue that disconnected from access point or on receiving threads.
# ja:切断発生時通知イベント（サーバー側からの切断または受信スレッドで異常発生時）
# </summary>
# <param name="msg">message</param>
# <param name="st">status</param>
notifyReceivedDisconnect = lambda{|msg, st|
    puts("{%s}, Status[0x%02x]" % [msg, st])
}

# <summary>
# en:notify when occurred issue for some reasons(except disconnect).
# ja:通信障害（切断以外）発生時通知イベント
# </summary>
# <param name="msg">message</param>
# <param name="msg_id">message id</param>
# <param name="err_code">error code</param>
notifyCommunicatonMessage = lambda{|msg, msg_id, err_code|
    puts("Message ID[0x%02x], Error Code[0x%02x], %s" % [msg_id, err_code, msg])
}

# <summary>
# en:notify when received a complition notice that made a speech.
# ja:読み上げ完了通知メッセージ受信時イベント
# </summary>
# <param name="result">submit result</param>
# <param name="time_stamp">time stamp</param>
rcvNotifyCompleteSpeech = lambda{|result, time_stamp|
    puts("%s：result[0x%02x], time stamp[%s]" % [strings[:rec_comp], result, time_stamp])
}

# <summary>
# en:notify when received a complition notice that made a speech.
# ja:音声認識コマンド送信メッセージ受信時イベント
# </summary>
# <param name="cmdInfo">HzSpeechRecognitionCommandInfo class object</param>
rcvSendSpeechRecognitionCommand = lambda{|cmdInfo|
    puts("%s : Ip[%s], Port[%d], Mode[%d], Command[%s], Timestamp[%s]" % [strings[:rcv_command], cmdInfo.IpAddr, cmdInfo.Port, cmdInfo.Mode, cmdInfo.Command, cmdInfo.Timestamp])
    #send speech data.
    if (cmdInfo.Command.casecmp(strings[:weather_command]) == 0)
        msg = strings[:weather_msg]
    else
        msg = strings[:invalid_command] % [cmdInfo.Command]
    end
    sampleDemo.serverPort = cmdInfo.Port
    sampleDemo.serverIp = cmdInfo.IpAddr
    sampleDemo.sendSpeechData(strings[:my_locate] + msg)
}


###############################################################
# en:create instance of demo class（regist callback functions）
# ja:デモクラスのインスタンス生成（コールバック関数の登録）
###############################################################
sampleDemo = HzSampleDemo.new(rcvNotifyCompleteSpeech, notifyCommunicatonMessage, notifyReceivedDisconnect, rcvSendSpeechRecognitionCommand)

###############################################################
#sampleDemo = HzSampleDemo.new()
# en:set some informations about access point.
# ja:接続先情報の設定
###############################################################
sampleDemo.serverPort = 46000
sampleDemo.serverIp = "192.168.1.2"

###############################################################
# en:set listener port to receive messages of asynchronous.
# ja:非同期メッセージ受信Listener portの設定
###############################################################
sampleDemo.receivePort = 46100


###############################################################
# en:If you want to receive voice command, need to Start listener.
#    Also if you want to decrypt that be encrypted voice command, 
#    need to set 'ReceivedDataDecryptionKey' property.
# ja:音声認識コマンドを受信する場合は、Listenerを起動する。
#    また、暗号化されたコマンドを受信する場合は、ReceivedDataDecryptionKey
#    に複合化キーをセットする。
###############################################################
sampleDemo.rcvDataDecryptionKey = "test23key"
sampleDemo.startAsynchronousListener()


###############################################################
# en:start main proccesing
# ja:メイン処理開始
###############################################################
Thread.abort_on_exception = true
th_rss = nil    #thread object for rss
print menu.chomp
while s = STDIN.gets.strip
    begin
        s == "" ? num = -1 : num = s.to_i
    rescue Exception => ex
        num = -2
    ensure
    end
    # en: you can make a set if you need authentication by account and encryption.
    # ja:アカウント認証と暗号化を行いたい場合は、下記情報を設定する。
    #account = "Raspberry Pi"
    #passwd = "test123Passwd"
    #encKey = "12345user1234"

    case num
        when 0 
            print strings[:input_data]
            msg = STDIN.gets.strip
            puts msg
            sampleDemo.sendSpeechData(msg)
            #sampleDemo.sendSpeechData(msg, account, passwd, encKey)
        when 1 
            begin
                rss_url = strings[:rss_url]
                th_rss = Thread.start(rss_url) do |u|
                    loop do
                        rss = RSS::Parser.parse(u)
                        rss_data = ""
                        rss.items.each{|item|
                            if lang == "ja"
                                rss_data << item.title.gsub(/ /, '、') + "\n"
                            else
                                rss_data << item.title + "\n"
                            end
                        }
                        puts "\n\n" + rss_data + "\n\n"
                        sampleDemo.sendSpeechData(rss_data)
                        #sampleDemo.sendSpeechData(rss_data, account, passwd, encKey)
                        #interval time(15 minutes)
                        sleep(900)
                    end
                end
            rescue Exception => ex
                puts strings[:end_rss]
                break
            ensure
            end
        when 2
            sampleDemo.cancelAsynchronousListener
            puts strings[:stop_listener]
        when 3
            sampleDemo.stopSendSpeechData
            puts strings[:cancel_send]
        when 4
            sampleDemo.receiveAckTimeOut
        when 5
            puts strings[:completion_notice]
            cmd = STDIN.gets.strip
            sampleDemo.setCompletionNoticeNecessity(cmd)
        when 99
            puts "completed demo."
            if(th_rss != nil && th_rss.alive?)
                # en:kill a thread to send a rss news data.
                # ja:RSSニュースデータ送信スレッド終了
                Thread.kill(th_rss)
            end
            break 
        when -1
        else
            puts strings[:irregular_num]
    end 
    print menu.chomp
end

