#!/usr/bin/ruby
# -*- coding: utf-8 -*-
require "haruzira_sdk"

cltTcpComm = ClientTcpCommunication.new
cltTcpComm.ServerPortNo = 46000             #port number of the destination
cltTcpComm.ServerIP = "192.168.1.6"         #ip address of the destination
cltTcpComm.ReqSendDataText = "Hello i am Haruzira. Thank you!"
timeStamp = cltTcpComm.sendSpeechDataEx
if(timeStamp != nil)
    puts("success. timestamp[%s]" % [timeStamp])
else
    puts("failed to send.")
end
