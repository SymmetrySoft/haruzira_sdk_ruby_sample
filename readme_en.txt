===============================================================================================
= Haruzira SDK for Ruby Sample Program   Copyright (c) 2016 Symmetry Soft
===============================================================================================
1. Overview
    For performing network communication with UWP APP "Haruzira", is a sample program that describes how to use the SDK for Ruby.


2. System Requirements
    It will to work.  Ruby version 1.9.X or later, and Gem to work if the installation environment.
    Environment that confirmed the operation on our side.(OS and Ruby version)
    a)Raspberry Pi B+(Raspbian 7.11) : ruby version 1.9.3 and 2.3.1
    b)Intel core i7 (Windows 10 anniversary 64bit): ruby version 2.3.0


3. Preparation
    1)Install a gem package, enter "gem install haruzira_sdk".

    2)You need to modify of some informations for the communication before the sample program start-up.
      a access point needs Ip address and port number.
      and a connection source needs a listener port number to receive asynchronous messages.
      
      You open SendTextDemo.rb by editor. and have to modify some values on the line number 137, 138 and 144.
      Number 46000 is a default port number for access point.
      and 46100 is a default port number to receive asynchronous messages.
      
      sampleDemo.serverPort = 46000           :- default port(If you have duplicate port, will be changed to any number)
      sampleDemo.serverIp = "192.168.1.6"     :- IP Address for access point.
      sampleDemo.receivePort = 46100          :- default port(If you have duplicate port, will be changed to any number)
      *UWP APP can not be basically that loopback(127.0.0.1). you need to work start-up on the other device or a virtual PC.

	3)If you want to receive voice command from Haruzira, need to set the Haruzira's following menu in advance.
      - 'Management of the Remote Devices' 
	        Device name: "Weather Information"
	        Encryption key: "test23key" for sample app. If you no need encryption, not enter any.
	        Encryption algorithm: "AES-CbcPkcs7"
	        IP address: Ex. "192.168.1.5"
	        Port number: default port "46100"

      - 'Management of the Remote Voice Commands' 
	        Command name: "Weather Information"
		    Recognition phrases: such as "weather;tell me status;temperature" etc. (free words)
		    Description: optional

    4)to start up "Haruzira" on the device for access point. and run the server.


4.How to Start up
    enter "ruby SendTextDemo.rb"
    *1)you can to operation on japanes mode, if add option "ja".
       ("en" or no option, English mode)
    *2)It will take some time, if start up by poor cpu or by like old type of Raspberry Pi. 
       (I was taken about 10 seconds and more by old type of Raspberry Pi B+.)
       Because a RSS module is a very heavy. Therefore it is spent many process times by "require rss" process.


5.How to Operation
    After start up, a menu is displayed on the console.
    Input a menu number. and make a operating as needed.
    Examples)
    1)Select "[0]send a speech data". and Input for send text.  and after send, it will be made a speech by Haruzira.
    2)Select "[1]send a rss news data(sending will repeat at 15 minutes interval)". and after send, it will be made a speech by Haruzira.


6.About SDK
    It is possible to control the speech synthesis engine, if be modified various speech options.
    
    Examples)
    1)It is possible to change the speech synthesis engine, if modify language code or gender option. 
    2)It is possible to repeat reproduce, if modify repeat count option.
    3)It is possible to make a speech by interrupt when is reproducing in priority of "Normal", if modify priority option to "High".
    4)It is possible to make a secure communication by using account authentication or encryption data, if modify account information or encryption key option.
    *if you need more information about SDK, make reference to a SDK manual or analyze sample program.
     https://haruzirasdke.wpblog.jp/


