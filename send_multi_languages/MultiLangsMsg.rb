#!/usr/bin/ruby
# -*- coding: utf-8 -*-
require "haruzira_sdk"

class MultiLangsMsg

    # <summary>
    # Construct
    # </summary>
    # <param name="pmsg">a messsage text</param>
    # <param name="plocale">locale ID(language code)</param>
    # <param name="pgender">gender(HzSpeechGender)</param>
    def initialize(pmsg = nil, plocale = nil, pgender = nil)
        @msg = ""
        @locale = 0x0409
        @gender = HzSpeechGender::Female
        if (pmsg != nil)
            @msg = pmsg
        end
        if (plocale != nil)
            @locale = plocale
        end
        if (pgender != nil)
            @gender = pgender
        end
    end

    attr_accessor   :msg, :locale, :gender

end