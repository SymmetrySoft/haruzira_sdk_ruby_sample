#!/usr/bin/ruby
# -*- coding: utf-8 -*-
require "timeout"
require "haruzira_sdk"
require "./MultiLangsMsg"

class SendMessage
    @@msgs = []
    @@msgs.push(MultiLangsMsg.new("この電車は、まもなく新宿駅に停車します。", 0x0411, HzSpeechGender::Female))
    @@msgs.push(MultiLangsMsg.new("This train will soon stop at Shinjuku Station.", 0x0409, HzSpeechGender::Male))
    @@msgs.push(MultiLangsMsg.new("Ce train s'arrêtera bientôt à la gare de Shinjuku.", 0x040c, HzSpeechGender::Female))

    def initialize(ip, port)
        @clt_tcp_comm = ClientTcpCommunication.new
        @clt_tcp_comm.ServerPortNo = port #port number for access point
        @clt_tcp_comm.ServerIP = ip #ip address for access point
        @queWaitNotifyCompleteSpeech = Queue.new 

        ###############################################################
        # call back functions for haruzirasdk. called when occurred events on a network communication.
        ###############################################################
        # <summary>
        # notify when received a 'speech completion notice' message.
        # </summary>
        # <param name="result">submit result</param>
        # <param name="time_stamp">time stamp</param>
        @clt_tcp_comm.EvNotifyCompeteSpeech = lambda{|result, time_stamp|
            puts("recieved notify complete speech. time stamp[%s]" % [time_stamp])
            @queWaitNotifyCompleteSpeech.push("OK")
        }
        #end define call back function 
    end


    # <summary>
    # waiting for response within timeout limit.
    # </summary>
    # <param name="t_val">timeout value to wait for response.</param>
    # <param name="que">message queue object</param>
    def queuePopWithTimeout(t_val, que)
        ret = false
        begin
            Timeout.timeout(t_val) do
                qret = que.pop
            end
            ret = true
        rescue Timeout::Error => ex
            puts("occurred timeout in waiting for response.")
            ret = false
        ensure
            return ret
        end 
    end

    # <summary>
    # send messages.
    # </summary>
    def text2speech()
        begin
            for msg in @@msgs
                @queWaitNotifyCompleteSpeech.clear
                @clt_tcp_comm.ReqSendDataText = msg.msg
                @clt_tcp_comm.ReqSendDataSpeechLocaleId = msg.locale
                @clt_tcp_comm.ReqSendDataSpeechGender = msg.gender
                @clt_tcp_comm.ReqSendDataSpeechRepeat = 1
                timestamp = @clt_tcp_comm.sendSpeechDataEx()
                if(timestamp != nil)
                    puts("time stamp[%s]" % [timestamp])
                end
                queuePopWithTimeout(20, @queWaitNotifyCompleteSpeech)
            end
        rescue Exception => ex
            puts(ex)
        ensure
        end
    end

end

#========== main routine ===================
sendMsg = SendMessage.new("192.168.1.7", 46000)
sendMsg.text2speech()
