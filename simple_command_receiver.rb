require "time"
require "haruzira_sdk"
 
#create instance
clt_tcp_comm = ClientTcpCommunication.new
 
#variable for terminal event.
terminate_flg = false

#Callback funtion
rcvSendSpeechRecognitionCommand = lambda{|cmd_info|
    puts("Ip[%s], Port[%d], Command[%s], Timestamp[%s]" % [cmd_info.IpAddr, cmd_info.Port, cmd_info.Command, cmd_info.Timestamp])
 
    #check received command
    if (cmd_info.Command.casecmp("weather information") == 0)
        msg = "It's sunny today.\r\nBut, It will probably be rainy tomorrow."
    else
        msg = "Invalid command name. [%s]\r\nI can't work your command." % [cmd_info.Command]
    end
 
    #send speech data.
    clt_tcp_comm.ServerPortNo = cmd_info.Port
    clt_tcp_comm.ServerIP = cmd_info.IpAddr
    clt_tcp_comm.ReqSendDataText = msg
    clt_tcp_comm.sendSpeechDataEx
    #set terminate flag.
    terminate_flg = true
}
 
#regist callback function.
clt_tcp_comm.EvNotifySendSpeechRecognitionCommand = rcvSendSpeechRecognitionCommand
 
#Set listener port number and start listener.
clt_tcp_comm.ReceivePort = 46100
clt_tcp_comm.ReqSendDataCompletionNoticeNecessity = HzSpCompletionNoticeNecessity::NoNeed
clt_tcp_comm.startAsynchronousListener()

#waiting for until a command is received. 
loop do
    if(terminate_flg)
        puts("terminate this process.")
        break
    end

    sleep(1)
end
