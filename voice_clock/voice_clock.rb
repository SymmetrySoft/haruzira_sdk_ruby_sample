﻿#!/usr/bin/ruby
# -*- coding: utf-8 -*-
require "timeout"
require "date"
require "time"
require "haruzira_sdk"

# if it's necessary for you start as a daemon process.
#Process.daemon

#region VoiceClock is a class for sending current time and receiving voice commands.
class VoiceClock

    def initialize(ip, port)
        @alarm_interval = 10
        @flg_stop = false
        @flg_term = false
        @th_worker = nil

        #for sending clock message.
        @clt_tcp_comm = ClientTcpCommunication.new
        @clt_tcp_comm.ServerIP = ip
        @clt_tcp_comm.ServerPortNo = port
        @clt_tcp_comm.ReceivePort = 46210
        @clt_tcp_comm.ReqSendDataCompletionNoticeNecessity = HzSpCompletionNoticeNecessity::NoNeed
        @clt_tcp_comm.ReqSendDataSpeechRepeat = 1
        @clt_tcp_comm.startAsynchronousListener()
        @clt_tcp_comm.setTraceOutPut(false)

        #for receiving voice commands.
        @cmd_rcv_com = ClientTcpCommunication.new
        @cmd_rcv_com.ReceivePort = 46200
        @cmd_rcv_com.ReqSendDataCompletionNoticeNecessity = HzSpCompletionNoticeNecessity::NoNeed
        @cmd_rcv_com.startAsynchronousListener()

        ###############################################################
        # call back functions for haruzirasdk. called when occurred events on a network communication.
        ###############################################################
        notify_send_speech_recognition_command = lambda{|cmdInfo|
            puts("Ip[%s], Port[%d], Command[%s], Timestamp[%s]" % [cmdInfo.IpAddr, cmdInfo.Port, cmdInfo.Command, cmdInfo.Timestamp])
        
            #check received command
            announce = ""
            if (cmdInfo.Command.casecmp("start voice clock") == 0)
                @flg_stop = false
                announce = "音声時計を開始します。"
                if(@th_worker.stop?)
                    @th_worker.run
                end
            elsif (cmdInfo.Command.casecmp("stop voice clock") == 0)
                @flg_stop = true
                announce = "音声時計を停止しました。"
            elsif (cmdInfo.Command.casecmp("terminate voice clock process") == 0)
                @flg_term = true
                announce = "音声時計プロセスを終了しました。"
            else
                if (/^(?=.*インターバル)(?=.*[\d]+)(?=.*(変更|設定))/ =~ cmdInfo.Command)
                    vals = cmdInfo.Command.scan(/[\d]+/)
                    interval = vals[0].tr("０-９", "0-9").to_i
                    if(interval >= 1 && interval <= 60)
                        @alarm_interval = interval
                        announce = "送信インターバルを%d分に設定しました。" % [@alarm_interval]
                    else
                        announce = "送信インターバルは、１分～６０分の範囲で指定して下さい。[指定された値：%d分]" % [interval]
                    end
                elsif (/^(?=.*(男性|女性))(?=.*(変更|設定))/ =~ cmdInfo.Command)
                    vals = cmdInfo.Command.scan(/男性|女性/)
                    if (vals[0] == '女性')
                        @clt_tcp_comm.ReqSendDataSpeechGender = HzSpeechGender::Female
                        @cmd_rcv_com.ReqSendDataSpeechGender = HzSpeechGender::Female
                    else
                        @clt_tcp_comm.ReqSendDataSpeechGender = HzSpeechGender::Male
                        @cmd_rcv_com.ReqSendDataSpeechGender = HzSpeechGender::Male
                    end
                    announce = "%sに設定しました。" % vals[0]
                else
                    announce = "該当するコマンドが有りませんでした。"
                end
            end

            puts announce

            begin
                #send speech data.
                @cmd_rcv_com.ServerPortNo = cmdInfo.Port
                @cmd_rcv_com.ServerIP = cmdInfo.IpAddr
                @cmd_rcv_com.ReqSendDataText = announce
                @cmd_rcv_com.sendSpeechDataEx
                if(@flg_term)
                    Thread.kill(@th_worker)
                end
            rescue Exception => ex
                puts(ex)
            ensure
            end

        }

        # notify when occurred issue that disconnected from access point or on receiving threads.
        # <param name="msg">message</param>
        # <param name="st">status</param>
        notify_received_disconnect = lambda{|msg, st|
            puts("{%s}, Status[0x%02x]" % [msg, st])
            #@flg_stop = true
        }

        # notify when occurred issue for some reasons(except disconnect).
        # <param name="msg">message</param>
        # <param name="msg_id">message id</param>
        # <param name="err_code">error code</param>
        notify_communicaton_message = lambda{|msg, msg_id, err_code|
            puts("Message ID[0x%02x], Error Code[0x%02x], %s" % [msg_id, err_code, msg])
            #@flg_stop = true
        }
        #end define call back function 

        @cmd_rcv_com.EvNotifySendSpeechRecognitionCommand = notify_send_speech_recognition_command
        @cmd_rcv_com.EvNotifyMessageEvent = notify_communicaton_message
        @cmd_rcv_com.EvNotifyReceivedDisConnectEvent = notify_received_disconnect
        @clt_tcp_comm.EvNotifyMessageEvent = notify_communicaton_message
        @clt_tcp_comm.EvNotifyReceivedDisConnectEvent = notify_received_disconnect
    end

    attr_accessor   :flg_stop, :th_worker

    # thread worker to send voice clock messsage.
    def worker_clock
        Thread.abort_on_exception = true
        @th_worker = Thread.start{
            Thread.stop
            loop do
                begin
                    nowtime = Time.new
    
                    if(!@flg_stop && nowtime.sec == 0 && (nowtime.min % @alarm_interval == 0))
                        puts("alarm time.[%s]" % [nowtime.to_time])
                        @clt_tcp_comm.ReqSendDataText = "%02d:%02dです。" % [nowtime.hour, nowtime.min]
                        timeStamp = @clt_tcp_comm.sendSpeechDataEx
                        if(timeStamp != nil)
                            puts("success. timestamp[%s]" % [timeStamp])
                        else
                            puts("failed to send.")
                        end
                    end
                
                    sleep(1)
    
                    if(@flg_stop)
                        Thread.stop
                        @flg_stop = false
                    end
                
                rescue Exception => ex
                    puts(ex)
                    break
                ensure
                end
            end
        }
    end

end
#endregion

#========== main routine ===================
#create instance. (Haruzira's ip and port)
voice_clock = VoiceClock.new("192.168.1.7", 46000)

#start working thread.
voice_clock.worker_clock()

def exit_signal_recieved(vc)
    Thread.kill(vc.th_worker)
    exit
end

Signal.trap(:INT){
    puts("received interrupt signal. (ctrl+c)")
    exit_signal_recieved(voice_clock)
}

Signal.trap(:TSTP){
    puts("received terminal stop signal. (ctrl+z)")
    exit_signal_recieved(voice_clock)
}

Signal.trap(:TERM){
    puts("received termination signal. (kill -15)")
    exit_signal_recieved(voice_clock)
}

#waiting for working thread until it complete.
voice_clock.th_worker.join
